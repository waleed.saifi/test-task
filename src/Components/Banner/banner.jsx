import { Grid, Typography } from '@mui/material';
import React from 'react';
import './banner.css';
export default function banner({ data }) {
  return (
    <Grid className="banner_main">
      <Typography className="banner_typo">{data}</Typography>
    </Grid>
  );
}
