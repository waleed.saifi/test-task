import { Grid, Typography } from "@mui/material";
import React from "react";
import Box from "@mui/material/Box";
import Rating from "@mui/material/Rating";
import "./card.css";

export default function Card({ data }) {
  const { img, description, title, title2, price } = data;
  const [value, setValue] = React.useState(2);

  return (
    <Grid container className="card_main">
      <img className="card_img" src={img} width="100%" />
      <div className="card_title">
        <Typography className="title">{title}</Typography>
      </div>
      <div className="card_bottom">
        <Typography className="title_2">{title2}</Typography>
        <Typography className="card_des">{description}</Typography>
        <Typography className="price">From ${price}</Typography>
        <Grid item container justifyContent="space-between">
          <Grid item>
            <div className="rating_div">
              <Rating
                className="rating"
                name="read-only"
                value={value}
                readOnly
              />
              <Typography className="rating_typo">2 Reviews</Typography>
            </div>
          </Grid>
          <Grid item>
            <img
              src="https://expeditionsconnect.com/static/media/skills3.cdd82111.svg"
              width="35px"
            />
          </Grid>
        </Grid>
      </div>
    </Grid>
  );
}
