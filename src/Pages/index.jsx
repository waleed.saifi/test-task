import React, { useEffect } from "react";
import "../App.css";
import { Container, Grid } from "@mui/material";
import Card from "../Components/Card/card";
import Banner from "../Components/Banner/banner";
import data from "../Data/data.js";

export default function Index() {
  const [featuredTrips, setFeaturedTrips] = React.useState([]);
  const [regularTrips, setRegularTrips] = React.useState([]);

  useEffect(() => {
    setFeaturedTrips(data.filter((obj) => obj.isFeatured));
    setRegularTrips(data.filter((obj) => !obj.isFeatured));
  }, []);

  return (
    <Container>
      <Grid>
        <Banner data={"Featured Trips"} />
      </Grid>
      <Grid container className="main_cont" spacing={2}>
        {featuredTrips.map((trips) => (
          <Grid item xs={12} md={6} lg={4}>
            <Card data={trips} />
          </Grid>
        ))}
      </Grid>
      <Grid>
        <Banner data={"Regular Trips"} />
      </Grid>
      <Grid container className="main_cont" spacing={2}>
        {regularTrips.map((trips) => (
          <Grid item xs={12} md={6} lg={4}>
            <Card data={trips} />
          </Grid>
        ))}
      </Grid>
    </Container>
  );
}
