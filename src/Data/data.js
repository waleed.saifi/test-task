const data = [
  {
    img: "https://expeditions-connect-bucket.s3.eu-central-1.amazonaws.com/cover/1604325094053.jpg",
    title: "Featured post 1",
    title2: "FEATURED POST 1",
    description:
      "Are you keen to continue your SCUBA adventure but don’t have much time? Qatar Marine has the perfect",
    price: "185",
    isFeatured: true,
  },
  {
    img: "https://expeditions-connect-bucket.s3.eu-central-1.amazonaws.com/cover/1604325094053.jpg",

    title: "Featured post 2",
    title2: "FEATURED POST 2",
    description:
      "Are you keen to continue your SCUBA adventure but don’t have much time? Qatar Marine has the perfect",
    price: "185",
    isFeatured: true,
  },

  {
    img: "https://expeditions-connect-bucket.s3.eu-central-1.amazonaws.com/cover/1604325094053.jpg",

    title: "2 DAYS QATAR SCUBA-DIVING",
    title2: "PADI ADVENTURE DIVER",
    description:
      "Are you keen to continue your SCUBA adventure but don’t have much time? Qatar Marine has the perfect",
    price: "185",
    isFeatured: false,
  },
  {
    img: "https://expeditions-connect-bucket.s3.eu-central-1.amazonaws.com/cover/1604325094053.jpg",

    title: "2 DAYS QATAR SCUBA-DIVING",
    title2: "PADI ADVENTURE DIVER",
    description:
      "Are you keen to continue your SCUBA adventure but don’t have much time? Qatar Marine has the perfect",
    price: "185",
    isFeatured: false,
  },
  {
    img: "https://expeditions-connect-bucket.s3.eu-central-1.amazonaws.com/cover/1604325094053.jpg",

    title: "2 DAYS QATAR SCUBA-DIVING",
    title2: "PADI ADVENTURE DIVER",
    description:
      "Are you keen to continue your SCUBA adventure but don’t have much time? Qatar Marine has the perfect",
    price: "185",
    isFeatured: false,
  },
  {
    img: "https://expeditions-connect-bucket.s3.eu-central-1.amazonaws.com/cover/1604325094053.jpg",

    title: "2 DAYS QATAR SCUBA-DIVING",
    title2: "PADI ADVENTURE DIVER",
    description:
      "Are you keen to continue your SCUBA adventure but don’t have much time? Qatar Marine has the perfect",
    price: "185",
    isFeatured: false,
  },
  {
    img: "https://expeditions-connect-bucket.s3.eu-central-1.amazonaws.com/cover/1604325094053.jpg",

    title: "2 DAYS QATAR SCUBA-DIVING",
    title2: "PADI ADVENTURE DIVER",
    description:
      "Are you keen to continue your SCUBA adventure but don’t have much time? Qatar Marine has the perfect",
    price: "185",
    isFeatured: false,
  },
  {
    img: "https://expeditions-connect-bucket.s3.eu-central-1.amazonaws.com/cover/1604325094053.jpg",

    title: "2 DAYS QATAR SCUBA-DIVING",
    title2: "PADI ADVENTURE DIVER",
    description:
      "Are you keen to continue your SCUBA adventure but don’t have much time? Qatar Marine has the perfect",
    price: "185",
    isFeatured: false,
  },
  {
    img: "https://expeditions-connect-bucket.s3.eu-central-1.amazonaws.com/cover/1604325094053.jpg",

    title: "Featured post 3",
    title2: "FEATURED POST 3",
    description:
      "Are you keen to continue your SCUBA adventure but don’t have much time? Qatar Marine has the perfect",
    price: "185",
    isFeatured: true,
  },
];
module.exports = data;
